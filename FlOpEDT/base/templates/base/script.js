var url_csv = "{% url "base:fetch_cost" department year week %}";
    $.ajax({
        type: "GET",
        dataType: 'text',
        url: url_csv,
        async: true,
        contentType: "text/csv",
        success: function(msg, req) {
            console.log(msg); 
            cost = d3.csvParse(msg, translate_costs_from_csv);
            console.log(cost);
            var table = document.getElementById("table");
            for(i=0;i<cost.length; i++){
                table.appendChild("<tr><td>"+cost[i].groupe+"</tr></td>")
            }
        },
        error: function(msg) {
            console.log("error");
        }
    });

    function translate_costs_from_csv(d) {
        var cost = {
 
            group : d.groupe,
            promotion : d.promo,
            week : d.semaine,
            year : d.an,
            cost : d.valeur,
        };
        return cost;
    }
